<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'reff',
        'code',
        'name',
        'hp',
        'amount',
        'expired',
        'status',
        'paid',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->amount = $model->amount + 2500;
            $model->code = "8834{$model->hp}";
            $model->status = "unpaid";
        });
    }
}

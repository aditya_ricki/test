<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    use HasFactory;

    protected $fillable = [
        'reff',
        'code',
        'name',
        'hp',
        'amount',
        'expired',
        'status',
        'paid',
    ];
}

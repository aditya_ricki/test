<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Traits\Response;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Jobs\BackupJob;

class OrderController extends Controller
{
    use Response;

    public function order(Request $request)
    {
        try {
            $request->validate([
                'amount' => 'required|numeric|min:1',
                'expired' => 'required|date|after_or_equal:' . now(),
                'name' => 'required',
                'hp' => 'required',
                'reff' => 'required',
            ]);

            $order = Order::create($request->all());

            return $this->success('Order berhasil', [
                'amount' => $order->amount,
                'reff' => $order->reff,
                'expired' => $order->expired,
                'name' => $order->name,
                'code' => $order->code,
            ]);
        } catch (\Throwable $th) {
            return $this->error($th, $th->getCode());
        }
    }

    public function payment(Request $request)
    {
        try {
            $request->validate([
                'reff' => 'required',
            ]);

            $order = Order::where('reff', $request->reff)
                ->firstOrFail();

            if ($order->status == 'paid') {
                throw new \Exception("Expired payment", 403);
            }

            $order->update([
                'paid' => now(),
                'status' => 'paid',
            ]);

            dispatch(new BackupJob($order));

            return $this->success('Pembayaran berhasil', [
                'amount' => $order->amount,
                'reff' => $order->reff,
                'name' => $order->name,
                'code' => $order->code,
                'status' => $order->status,
            ]);
        } catch (\Throwable $th) {
            return $this->error($th, $th->getCode());
        }
    }

    public function status(Request $request)
    {
        try {
            $request->validate([
                'reff' => 'required',
            ]);

            $order = Order::where('reff', $request->reff)
                ->select('amount', 'reff', 'name', 'expired', 'paid', 'code', 'status')
                ->firstOrFail();

            return $this->success('Status order', $order);
        } catch (\Throwable $th) {
            return $this->error($th, $th->getCode());
        }
    }
}

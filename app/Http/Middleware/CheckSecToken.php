<?php

namespace App\Http\Middleware;

use App\Traits\Response;
use Closure;
use Illuminate\Http\Request;

class CheckSecToken
{
    use Response;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            if ($request->header('Sec-Token') != date('yyyyMMdd')) {
                throw new \Exception("Invalid Sec-Token", 403);
            }

            return $next($request);
        } catch (\Throwable $th) {
            return $this->error($th, $th->getCode());
        }
    }
}

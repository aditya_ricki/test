<?php

namespace App\Traits;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

trait Response
{
    public function success($message = null, $data = null, $code = 200)
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function error($exception, $code = 500)
    {
        Log::error($exception->getMessage());

        if ($exception instanceof ModelNotFoundException) {
            $message = 'Data tidak ditemukan';
            $code = 403;
        } elseif ($exception instanceof ValidationException) {
            $message = $exception->validator->errors()->first();
        } elseif ($exception instanceof QueryException) {
            $message = 'Terjadi kesalahan query';
        } else {
            $message = $exception->getMessage();
        }

        return response()->json([
            'message' => $message,
            'data' => null,
        ], $code);
    }
}

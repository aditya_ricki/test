<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'check.sectoken'], function () {
    Route::get('order', [OrderController::class, 'order']);
    Route::get('payment', [OrderController::class, 'payment']);
    Route::get('status', [OrderController::class, 'status']);
});

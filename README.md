###Requirements

-   php 8.0+

###Instalation

Install all dependencies

```bash
composer install
```

```bash
npm install && npm run dev
```

```bash
npm run dev
```

###Clear config, cache and etc.

```bash
php artisan optimize
```

###Database

-   Create database with name: `test`
-   Change DB_USERNAME and DB_PASSWORD in `.env`
-   Running migration and seeding

```bash
php artisan migrate --seed
```

###Running application

```bash
php artisan serve
```
